package piscot.springframework.locationinfo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import piscot.springframework.locationinfo.domain.Offer;;

public interface OfferRepository extends JpaRepository<Offer, Long> {

}