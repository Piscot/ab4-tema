package piscot.springframework.locationinfo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import piscot.springframework.locationinfo.domain.ApplicationUser;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {

	ApplicationUser findByUsername(String username);
}