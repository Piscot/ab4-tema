package piscot.springframework.locationinfo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import piscot.springframework.locationinfo.domain.Activity;;

public interface ActivityRepository extends JpaRepository<Activity, Long> {

}