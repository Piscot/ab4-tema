package piscot.springframework.locationinfo.api.v1.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.Data;

@Data
public class ActivityDTO {
	private Long id;
	private String name;
	private Date startDate;
	private Date endDate;

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setStartDate(String startDate) throws ParseException {
		this.startDate = new SimpleDateFormat("d/M/yyyy").parse(startDate);
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setEndDate(String endDate) throws ParseException {
		this.endDate = new SimpleDateFormat("d/M/yyyy").parse(endDate);
	}
}