package piscot.springframework.locationinfo.api.v1.model;

import lombok.Data;

@Data
public class LocationDTO {
	private Long id;
	private String name;
	private String country;
	private String region;
	private String locality;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}