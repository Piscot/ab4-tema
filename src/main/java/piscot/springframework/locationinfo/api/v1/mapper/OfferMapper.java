package piscot.springframework.locationinfo.api.v1.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import piscot.springframework.locationinfo.api.v1.model.OfferDTO;
import piscot.springframework.locationinfo.domain.Offer;

@Mapper
public interface OfferMapper {
	OfferMapper INSTANCE = Mappers.getMapper(OfferMapper.class);

	OfferDTO offerToOfferDTO(Offer offer);

	Offer offerDtoToOffer(OfferDTO offerDTO);
}