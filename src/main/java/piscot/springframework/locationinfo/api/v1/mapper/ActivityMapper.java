package piscot.springframework.locationinfo.api.v1.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import piscot.springframework.locationinfo.api.v1.model.ActivityDTO;
import piscot.springframework.locationinfo.domain.Activity;

@Mapper
public interface ActivityMapper {
	ActivityMapper INSTANCE = Mappers.getMapper(ActivityMapper.class);

	ActivityDTO activityToActivityDTO(Activity activity);

	Activity activityDtoToActivity(ActivityDTO activityDTO);
}