package piscot.springframework.locationinfo.api.v1.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class OfferDTO {
	private Long id;
	private BigDecimal cost;
	private ActivityDTO activity;
	private LocationDTO location;
}