package piscot.springframework.locationinfo.api.v1.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LocationListDTO {
	List<LocationDTO> locations;
}