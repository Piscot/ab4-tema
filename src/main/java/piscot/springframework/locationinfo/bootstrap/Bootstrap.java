package piscot.springframework.locationinfo.bootstrap;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import piscot.springframework.locationinfo.domain.*;
import piscot.springframework.locationinfo.repositories.*;

@Component
public class Bootstrap implements ApplicationListener<ContextRefreshedEvent> {
	private final LocationRepository locationRepository;
	private final ActivityRepository activityRepository;
	private final OfferRepository offerRepository;
	private final ApplicationUserRepository userRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public Bootstrap(LocationRepository locationRepository, ActivityRepository activityRepository,
			OfferRepository offerRepository, ApplicationUserRepository userRepository,
			BCryptPasswordEncoder bCryptPasswordEncoder) {

		this.locationRepository = locationRepository;
		this.activityRepository = activityRepository;
		this.offerRepository = offerRepository;
		this.userRepository = userRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		try {
			seedDatabase();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void seedDatabase() throws Exception {
		ApplicationUser firstUser = new ApplicationUser();
		firstUser.setUsername("administrator");
		firstUser.setEmail("administrator@gmail.com");
		firstUser.setPassword(bCryptPasswordEncoder.encode("pass"));
		userRepository.save(firstUser);
		// 1
		Location firstLocation = new Location();
		firstLocation.setName("Poiana Brasov");
		firstLocation.setCountry("Romania");
		firstLocation.setRegion("montana");
		firstLocation.setLocality("Prahova");

		Activity firstActivity = new Activity();
		firstActivity.setName("Tiroliana");
		String sDate1 = "02/05/2020";
		Date date1 = new SimpleDateFormat("d/M/yyyy").parse(sDate1);
		String sDate2 = "05/10/2020";
		Date date2 = new SimpleDateFormat("d/M/yyyy").parse(sDate2);
		firstActivity.setStartDate(date1);
		firstActivity.setEndDate(date2);

		locationRepository.save(firstLocation);
		activityRepository.save(firstActivity);

		Offer firstOffer = new Offer();
		firstOffer.setCost(BigDecimal.valueOf(80));
		firstOffer.setActivity(firstActivity);
		firstOffer.setLocation(firstLocation);

		offerRepository.save(firstOffer);

		// 2
		Location secondLocation = new Location();
		secondLocation.setName("Ranca");
		secondLocation.setCountry("Romania");
		secondLocation.setRegion("montana");
		secondLocation.setLocality("Gorj");

		Activity secondActivity = new Activity();
		secondActivity.setName("Ski");
		String sDate3 = "07/12/2020";
		Date date3 = new SimpleDateFormat("d/M/yyyy").parse(sDate3);
		String sDate4 = "15/03/2021";
		Date date4 = new SimpleDateFormat("d/M/yyyy").parse(sDate4);
		secondActivity.setStartDate(date3);
		secondActivity.setEndDate(date4);

		locationRepository.save(secondLocation);
		activityRepository.save(secondActivity);

		Offer secondOffer = new Offer();
		secondOffer.setCost(BigDecimal.valueOf(150));
		secondOffer.setActivity(secondActivity);
		secondOffer.setLocation(secondLocation);

		offerRepository.save(secondOffer);

		// 3
		Location thirdLocation = new Location();
		thirdLocation.setName("Arieseni");
		thirdLocation.setCountry("Romania");
		thirdLocation.setRegion("montana");
		thirdLocation.setLocality("Alba");

		Activity thirdActivity = new Activity();
		thirdActivity.setName("Ski");
		String sDate5 = "01/12/2020";
		Date date5 = new SimpleDateFormat("d/M/yyyy").parse(sDate5);
		String sDate6 = "15/04/2021";
		Date date6 = new SimpleDateFormat("d/M/yyyy").parse(sDate6);
		thirdActivity.setStartDate(date5);
		thirdActivity.setEndDate(date6);

		locationRepository.save(thirdLocation);
		activityRepository.save(thirdActivity);

		Offer thirdOffer = new Offer();
		thirdOffer.setCost(BigDecimal.valueOf(100));
		thirdOffer.setLocation(thirdLocation);
		thirdOffer.setActivity(thirdActivity);

		offerRepository.save(thirdOffer);

		// 4
		Location fourthLocation = new Location();
		fourthLocation.setName("Bran");
		fourthLocation.setCountry("Romania");
		fourthLocation.setRegion("montana");
		fourthLocation.setLocality("Brasov");

		Activity fourthActivity = new Activity();
		fourthActivity.setName("ATV");
		String sDate7 = "15/1/2020";
		Date date7 = new SimpleDateFormat("d/M/yyyy").parse(sDate7);
		String sDate8 = "15/12/2020";
		Date date8 = new SimpleDateFormat("d/M/yyyy").parse(sDate8);
		fourthActivity.setStartDate(date7);
		fourthActivity.setEndDate(date8);

		locationRepository.save(fourthLocation);
		activityRepository.save(fourthActivity);

		Offer fourthOffer = new Offer();
		fourthOffer.setCost(BigDecimal.valueOf(175.50));
		fourthOffer.setLocation(fourthLocation);
		fourthOffer.setActivity(fourthActivity);

		offerRepository.save(fourthOffer);

		// 5
		Location fifthLocation = new Location();
		fifthLocation.setName("Rucar");
		fifthLocation.setCountry("Romania");
		fifthLocation.setRegion("deluroasa");
		fifthLocation.setLocality("Arges");

		Activity fifthActivity = new Activity();
		fifthActivity.setName("ATV");
		String sDate9 = "15/1/2020";
		Date date9 = new SimpleDateFormat("d/M/yyyy").parse(sDate9);
		String sDate10 = "15/12/2020";
		Date date10 = new SimpleDateFormat("d/M/yyyy").parse(sDate10);
		fifthActivity.setStartDate(date9);
		fifthActivity.setEndDate(date10);

		locationRepository.save(fifthLocation);
		activityRepository.save(fifthActivity);

		Offer fifthOffer = new Offer();
		fifthOffer.setCost(BigDecimal.valueOf(130));
		fifthOffer.setLocation(fifthLocation);
		fifthOffer.setActivity(fifthActivity);

		offerRepository.save(fifthOffer);

		// 6
		Location sixthLocation = new Location();
		sixthLocation.setName("Rucar");
		sixthLocation.setCountry("Romania");
		sixthLocation.setRegion("deluroasa");
		sixthLocation.setLocality("Arges");

		Activity sixthActivity = new Activity();
		sixthActivity.setName("Tiroliana");
		String sDate11 = "15/5/2020";
		Date date11 = new SimpleDateFormat("d/M/yyyy").parse(sDate11);
		String sDate12 = "15/10/2020";
		Date date12 = new SimpleDateFormat("d/M/yyyy").parse(sDate12);
		sixthActivity.setStartDate(date11);
		sixthActivity.setEndDate(date12);

		locationRepository.save(sixthLocation);
		activityRepository.save(sixthActivity);

		Offer sixthOffer = new Offer();
		sixthOffer.setCost(BigDecimal.valueOf(180));
		sixthOffer.setLocation(sixthLocation);
		sixthOffer.setActivity(sixthActivity);

		offerRepository.save(sixthOffer);
	}
}