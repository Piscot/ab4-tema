package piscot.springframework.locationinfo.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;

import piscot.springframework.locationinfo.api.v1.mapper.OfferMapper;
import piscot.springframework.locationinfo.api.v1.model.OfferDTO;
import piscot.springframework.locationinfo.domain.Offer;
import piscot.springframework.locationinfo.domain.QActivity;
import piscot.springframework.locationinfo.domain.QLocation;
import piscot.springframework.locationinfo.domain.QOffer;
import piscot.springframework.locationinfo.repositories.ActivityRepository;
import piscot.springframework.locationinfo.repositories.LocationRepository;
import piscot.springframework.locationinfo.repositories.OfferRepository;

@Service
public class OfferServiceImpl implements OfferService {
	private OfferMapper offerMapper;
	private OfferRepository offerRepository;
	private LocationRepository locationRepository;
	private ActivityRepository activityRepository;

	@Autowired
	public void setOfferMapper(OfferMapper offerMapper) {
		this.offerMapper = offerMapper;
	}

	@Autowired
	public void setOfferRepository(OfferRepository offerRepository, LocationRepository locationRepository,
			ActivityRepository activityRepository) {
		this.offerRepository = offerRepository;
		this.locationRepository = locationRepository;
		this.activityRepository = activityRepository;
	}

	@Autowired
	EntityManager entityManager;

	@Override
	public OfferDTO getOfferById(Long id) {
		Offer offer = offerRepository.findById(id).get();
		return offerMapper.offerToOfferDTO(offer);
	}

	@Override
	public List<OfferDTO> getAllOffers(Map<String, String[]> params) throws ParseException {
		QOffer qoffer = QOffer.offer;
		QLocation qlocation = QLocation.location;
		QActivity qactivity = QActivity.activity;

		JPAQuery<?> query = new JPAQuery<Void>(entityManager);

		BooleanBuilder builder = new BooleanBuilder();

		if (params.containsKey("country")) {
			builder.and(qlocation.country.eq(params.get("country")[0]));
		}
		if (params.containsKey("region")) {
			builder.and(qlocation.region.eq(params.get("region")[0]));
		}
		if (params.containsKey("locality")) {
			builder.and(qlocation.locality.eq(params.get("locality")[0]));
		}
		if (params.containsKey("startDate")) {
			Date startDate = new SimpleDateFormat("MM-yyyy").parse(params.get("startDate")[0]);
			builder.and(qactivity.endDate.after(startDate));
		}
		if (params.containsKey("endDate")) {
			Date endDate = new SimpleDateFormat("MM-yyyy").parse(params.get("endDate")[0]);
			builder.and(qactivity.startDate.before(endDate));
		}
		if (params.containsKey("activities")) {
			builder.and(qactivity.name.in(params.get("activities")));
		}

		return query.select(qoffer).from(qoffer).innerJoin(qoffer.activity, qactivity)
				.innerJoin(qoffer.location, qlocation).where(builder).orderBy(qoffer.cost.asc()).fetch().stream()
				.map(offerMapper::offerToOfferDTO).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public OfferDTO createNewOffer(OfferDTO offerDTO) {

		Offer offer = offerMapper.offerDtoToOffer(offerDTO);
		locationRepository.save(offer.getLocation());
		activityRepository.save(offer.getActivity());

		Offer savedOffer = offerRepository.save(offer);

		OfferDTO returnDto = offerMapper.offerToOfferDTO(savedOffer);

		return returnDto;
	}

	@Override
	public OfferDTO updateOffer(Long id, OfferDTO offerDTO) {
		offerDTO.setId(id);
		Offer offer = offerMapper.offerDtoToOffer(offerDTO);

		Offer savedOffer = offerRepository.save(offer);

		OfferDTO returnDto = offerMapper.offerToOfferDTO(savedOffer);

		return returnDto;
	}

	@Override
	@Transactional
	public void deleteOfferById(Long id) {
		Offer offer = offerRepository.findById(id).get();

		locationRepository.deleteById(offer.getActivity().getId());
		activityRepository.deleteById(offer.getActivity().getId());
		offerRepository.deleteById(id);
	}
}