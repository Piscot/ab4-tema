package piscot.springframework.locationinfo.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import piscot.springframework.locationinfo.api.v1.mapper.LocationMapper;
import piscot.springframework.locationinfo.api.v1.model.LocationDTO;
import piscot.springframework.locationinfo.domain.Location;

import piscot.springframework.locationinfo.repositories.LocationRepository;
import piscot.springframework.locationinfo.services.LocationService;

@Service
public class LocationServiceImpl implements LocationService {
	private LocationMapper locationMapper;
	private LocationRepository locationRepository;

	@Autowired
	public void setLocationMapper(LocationMapper locationMapper) {
		this.locationMapper = locationMapper;
	}

	@Autowired
	public void setLocationRepository(LocationRepository locationRepository) {
		this.locationRepository = locationRepository;
	}

	@Override
	public List<LocationDTO> getAllLocations() {
		return locationRepository.findAll().stream().map(locationMapper::locationToLocationDTO)
				.collect(Collectors.toList());
	}

	@Override
	public LocationDTO getLocationById(Long id) {
		Location location = locationRepository.findById(id).get();
		return locationMapper.locationToLocationDTO(location);
	}

	@Override
	public LocationDTO createNewLocation(LocationDTO locationDTO) {
		Location location = locationMapper.locationDtoToLocation(locationDTO);

		Location savedLocation = locationRepository.save(location);

		LocationDTO returnDto = locationMapper.locationToLocationDTO(savedLocation);

		return returnDto;
	}

	@Override
	public LocationDTO updateLocation(Long id, LocationDTO locationDTO) {
		locationDTO.setId(id);
		Location location = locationMapper.locationDtoToLocation(locationDTO);

		Location savedLocation = locationRepository.save(location);

		LocationDTO returnDto = locationMapper.locationToLocationDTO(savedLocation);

		return returnDto;
	}

	@Override
	public void deleteLocationById(Long id) {
		locationRepository.deleteById(id);
	}
}