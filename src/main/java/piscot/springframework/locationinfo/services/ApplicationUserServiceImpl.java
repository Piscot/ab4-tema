package piscot.springframework.locationinfo.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import piscot.springframework.locationinfo.domain.ApplicationUser;
import piscot.springframework.locationinfo.repositories.ApplicationUserRepository;

@Service
public class ApplicationUserServiceImpl implements ApplicationUserService {
	final ApplicationUserRepository userRepository;

	public ApplicationUserServiceImpl(ApplicationUserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public Set<ApplicationUser> getUsers() {
		Set<ApplicationUser> usersSet = new HashSet<>();
		userRepository.findAll().iterator().forEachRemaining(usersSet::add);
		return usersSet;
	}
}