package piscot.springframework.locationinfo.services;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import piscot.springframework.locationinfo.api.v1.mapper.ActivityMapper;
import piscot.springframework.locationinfo.api.v1.model.ActivityDTO;
import piscot.springframework.locationinfo.domain.Activity;
import piscot.springframework.locationinfo.repositories.ActivityRepository;

@Service
public class ActivityServiceImpl implements ActivityService {
	private ActivityMapper activityMapper;
	private ActivityRepository activityRepository;

	@Autowired
	public void setActivityMapper(ActivityMapper activityMapper) {
		this.activityMapper = activityMapper;
	}

	@Autowired
	public void setActivityRepository(ActivityRepository activityRepository) {
		this.activityRepository = activityRepository;
	}

	@Override
	public List<ActivityDTO> getAllActivities() {
		return activityRepository.findAll().stream().map(activityMapper::activityToActivityDTO)
				.collect(Collectors.toList());
	}

	@Override
	public ActivityDTO getActivityById(Long id) {
		Activity activity = activityRepository.findById(id).get();
		return activityMapper.activityToActivityDTO(activity);
	}

	@Override
	public ActivityDTO createNewActivity(ActivityDTO activityDTO) throws ParseException {

		Activity activity = activityMapper.activityDtoToActivity(activityDTO);

		Activity savedActivity = activityRepository.save(activity);

		ActivityDTO returnDto = activityMapper.activityToActivityDTO(savedActivity);

		return returnDto;
	}

	@Override
	public void deleteActivityById(Long id) {
		activityRepository.deleteById(id);
	}
}