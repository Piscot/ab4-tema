package piscot.springframework.locationinfo.services;

import java.util.Set;

import piscot.springframework.locationinfo.domain.ApplicationUser;

public interface ApplicationUserService {
	Set<ApplicationUser> getUsers();
}