package piscot.springframework.locationinfo.services;

import java.text.ParseException;
import java.util.List;

import piscot.springframework.locationinfo.api.v1.model.ActivityDTO;

public interface ActivityService {

	ActivityDTO getActivityById(Long id);

	List<ActivityDTO> getAllActivities();

	ActivityDTO createNewActivity(ActivityDTO activityDTO) throws ParseException;

	void deleteActivityById(Long id);
}