package piscot.springframework.locationinfo.services;

import java.util.List;

import piscot.springframework.locationinfo.api.v1.model.LocationDTO;

public interface LocationService {
	List<LocationDTO> getAllLocations();

	LocationDTO getLocationById(Long id);

	LocationDTO createNewLocation(LocationDTO locationDTO);

	LocationDTO updateLocation(Long id, LocationDTO locationDTO);

	void deleteLocationById(Long id);
}