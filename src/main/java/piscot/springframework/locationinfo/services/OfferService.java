package piscot.springframework.locationinfo.services;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import piscot.springframework.locationinfo.api.v1.model.OfferDTO;

public interface OfferService {
	OfferDTO getOfferById(Long id);

	OfferDTO createNewOffer(OfferDTO offerDTO);

	OfferDTO updateOffer(Long id, OfferDTO offerDTO);

	void deleteOfferById(Long id);

	List<OfferDTO> getAllOffers(Map<String, String[]> params) throws ParseException;
}