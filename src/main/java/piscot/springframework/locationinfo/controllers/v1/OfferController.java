package piscot.springframework.locationinfo.controllers.v1;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import piscot.springframework.locationinfo.api.v1.model.OfferDTO;
import piscot.springframework.locationinfo.services.*;

@RestController
@RequestMapping("/api/offers")
public class OfferController {
	private final OfferService offerService;

	public OfferController(OfferService offerService) {
		this.offerService = offerService;
	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List<OfferDTO> index(HttpServletRequest request) throws ParseException {
		Map<String, String[]> params = request.getParameterMap();

		return offerService.getAllOffers(params);
	}

	@GetMapping({ "/{id}" })
	@ResponseStatus(HttpStatus.OK)
	public OfferDTO show(@PathVariable Long id) {
		return offerService.getOfferById(id);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public OfferDTO create(HttpServletRequest request, @RequestBody OfferDTO offerDTO) {

		return offerService.createNewOffer(offerDTO);
	}

	@PutMapping({ "/{id}" })
	@ResponseStatus(HttpStatus.OK)
	public OfferDTO update(@PathVariable Long id, @RequestBody OfferDTO offerDTO) {

		return offerService.updateOffer(id, offerDTO);
	}

	@DeleteMapping({ "/{id}" })
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable Long id) {
		offerService.deleteOfferById(id);
	}
}