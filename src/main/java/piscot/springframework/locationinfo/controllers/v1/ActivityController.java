package piscot.springframework.locationinfo.controllers.v1;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import piscot.springframework.locationinfo.api.v1.model.ActivityDTO;
import piscot.springframework.locationinfo.services.ActivityService;

@RestController
@RequestMapping("/api/activities")
public class ActivityController {
	private final ActivityService activityService;

	public ActivityController(ActivityService activityService) {
		this.activityService = activityService;
	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List<ActivityDTO> index() {
		return activityService.getAllActivities();
	}

	@GetMapping({ "/{id}" })
	@ResponseStatus(HttpStatus.OK)
	public ActivityDTO show(@PathVariable Long id) {
		return activityService.getActivityById(id);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ActivityDTO create(HttpServletRequest request, @RequestBody ActivityDTO activityDTO) throws ParseException {

		return activityService.createNewActivity(activityDTO);
	}

	@DeleteMapping({ "/{id}" })
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable Long id) {
		activityService.deleteActivityById(id);
	}
}