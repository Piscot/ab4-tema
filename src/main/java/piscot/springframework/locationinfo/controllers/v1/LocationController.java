package piscot.springframework.locationinfo.controllers.v1;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import piscot.springframework.locationinfo.api.v1.model.LocationDTO;
import piscot.springframework.locationinfo.services.LocationService;

@RestController
@RequestMapping("/api/locations")
public class LocationController {
	private final LocationService locationService;

	public LocationController(LocationService locationService) {
		this.locationService = locationService;
	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List<LocationDTO> index() {
		return locationService.getAllLocations();
	}

	@GetMapping({ "/{id}" })
	@ResponseStatus(HttpStatus.OK)
	public LocationDTO show(@PathVariable Long id) {
		return locationService.getLocationById(id);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public LocationDTO create(HttpServletRequest request, @RequestBody LocationDTO locationDTO) {

		return locationService.createNewLocation(locationDTO);
	}

	@PutMapping({ "/{id}" })
	@ResponseStatus(HttpStatus.OK)
	public LocationDTO update(@PathVariable Long id, @RequestBody LocationDTO locationDTO) {

		return locationService.updateLocation(id, locationDTO);
	}

	@DeleteMapping({ "/{id}" })
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable Long id) {
		locationService.deleteLocationById(id);
	}
}